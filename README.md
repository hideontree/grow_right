# Grow Right
[![growright](./logo.png)](https://www.growright.digital/)


## Project Overview
Grow Right is a revolutionary new job search information service platform that matches job searchers based on their talents and preferences. Job seekers only need to build a profile on the Grow Right platform, which includes a CV, wage expectations, and career goals, to receive job listings for relevant employment.

In terms of technological implementation, Grow Right members filter job applicants' information by keywords to match the best candidates with open positions using machine learning technology and natural language processing models.


## Project Purpose
Our work this semester is mainly divided into three parts: front-end page design, back-end architecture construction and algorithm design.



## Team Chart
| Name        | UID      | Role                | Part              |
|-------------|----------|---------------------|-------------------|
| Bryce  | -        | Client              | -                 |
| Peiyuan Yu    |u7543592   | Spokesperson | Front-end |
| Zixin Su  | u7425489   | Spokesperson | Back-end |
| Zhoushu He  | u5926302  | Member              |        Algorithm  |
| Bowen Hu    | u7152919  | Member              |          Back-end |
| Johnny Wang  | u7543414  | Member              |         Front-end   |
| Boyi Zhang   | u7340957  | Member              |          Algorithm  |
| Guanghao Li       |   u7560865       |   Member|        Front-end  |


## Project Management Navigation
- [Risk Log](https://trello.com/b/aSVLRHMx/risk-log)
- [Decision Log](https://trello.com/b/aD2hFgNx/decision-log)
- [Reflection Log](https://trello.com/b/WmeNxHQf/feedback-log)
- [Task management](https://trello.com/b/QAL5masH/careermatch2023s2)
- [Client Meeting](https://trello.com/b/fEZeGvFK/meetingclient)
- [Group Meeting](https://trello.com/b/E3BCqqb5/meetinggroup)
- [Tutorial](https://trello.com/b/g7zCs1rj/meetingtotorial)
- [User Story Map](https://miro.com/welcomeonboard/Ylp3SzFPcmxuWVk2SUR5YWcycWdjdktTdkE2bGVocTVmUGQ0dk1RalVxU1ZjQ3JId295Wm5Vdk9VZjFUbjdYenwzNDU4NzY0NTY0ODU1MDAxMjI4fDI=?share_link_id=591291830767)
- [Contribution Score Table](https://gitlab.com/hideontree/grow_right/-/blob/main/Documentation/Contribution%20Score%20table.md)



##  Audit 1
- [Audit 1 slides](https://gitlab.com/hideontree/grow_right/-/blob/main/Documentation/GrowRight_audit1.pptx)
- [Statement of Work](./Documentation/SOW-CareerMatch.pdf)
- [Constraint](./Documentation/Resource___Risk___Potential_Cost___Constraint.pdf?ref_type=heads)
- [Potential Cost](./Documentation/Potential_Cost_.pdf?ref_type=heads)
- [Project Client Map](./Documentation/Project_Client_Map_GrowRight.docx.pdf?ref_type=heads)


##  Audit 2
- [Audit 2 Slides](https://gitlab.com/hideontree/grow_right/-/blob/main/Documentation/GrowRight_audit2.pptx)

##  Audit 3
- [Audit 3 Slides](https://docs.google.com/presentation/d/1wZnC84V0bU7alG6en4gwVF-vzfnpDbps/edit#slide=id.p1)


##  Repository
[GitLab](https://gitlab.com/hideontree/grow_right)


##  Documentation
- [Statement of Work](./Documentation/SOW-CareerMatch.pdf)
- [Team Charter](./Documentation/Team_Charter.pdf)
