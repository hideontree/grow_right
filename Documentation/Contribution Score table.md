# Week 4

| item                                              | Zixin Su | Peiyuan Yu | Bowen Hu | Boyi Zhang | Jiangning Wang | Guanghao Li | Zhoushu He |
|---------------------------------------------------|:--------:|:----------:|:--------:|:----------:|:--------------:|:-----------:|:----------:|
| Attend client meeting - 0.11                      |   [1 ]    |    [1 ]     |   [ 1]    |    [ 1]     |      [1 ]       |     [ 1]     |    [1 ]     |
| Attend tutorial meeting - 0.11                    |   [ 1]    |    [1 ]     |   [ 1]    |    [ 1]     |      [ 1]       |     [ 1]     |    [1 ]     |
| Attend group meeting - 0.11                       |   [1 ]    |    [1 ]     |   [1 ]    |    [ 1]     |      [1 ]       |     [1 ]     |    [ 1]     |
| Communicate with client - 0.11                    |   [1 ]    |    [1 ]     |   [1 ]    |    [ 1]     |      [ 1]       |     [1 ]     |    [1 ]     |
| Communicate with tutor - 0.11                     |   [ 1]    |    [ 1]     |   [1 ]    |    [1 ]     |      [ ]       |     [ ]     |    [ ]     |
| Communicate with shadow team - 0.11               |   [1 ]    |    [ 1]     |   [1 ]    |    [ 1]     |      [ 1]       |     [ 1]     |    [1 ]     |
| Represent agenda in meeting - 0.33                |   [1 ]    |    [1 ]     |   [1 ]    |    [ 1]     |      [ ]       |     [ ]     |    [ ]     |
| Write down thought on own initiative - 0.5        |   [ 1]    |    [ 1]     |   [1 ]    |    [1 ]     |      [ 1]       |     [ 1]     |    [ ]     |
| Complete job after distribute in meetings - 0.5   |   [ 1]    |    [1 ]     |   [1 ]    |    [ 1]     |      [ 1]       |     [1 ]     |    [ 1]     |
| Own distributed job and reflected in Gitlab - 0.5 |   [1 ]    |    [1 ]     |   [1 ]    |    [1 ]     |      [1]       |     [1 ]     |    [ ]     |
| Positive in group chat and discussion - 0.8       |   [1 ]    |    [1 ]     |   [ 1]    |    [ 1]     |      [ ]       |     [ ]     |    [ ]     |
| Come up with own idea - 0.2                       |   [ 1]    |    [1 ]     |   [1 ]    |    [ 1]     |      [ ]       |     [ ]     |    [ ]     |
| Bonus for everyone - 1                            |   [x]    |    [x]     |   [x]    |    [x]     |      [x]       |     [x]     |    [x]     |
| Total score                                       |  5 |     5       |    5      |     5       |       3.56         |    3.56         |    2.56        |


# Week 5

| item                                              | Zixin Su | Peiyuan Yu | Bowen Hu | Boyi Zhang | Jiangning Wang | Guanghao Li | Zhoushu He |
|---------------------------------------------------|:--------:|:----------:|:--------:|:----------:|:--------------:|:-----------:|:----------:|
| Attend client meeting - 0.11                      |   [ 1]    |    [1 ]     |   [1 ]    |    [1 ]     |      [1 ]       |     [1 ]     |    [1 ]     |
| Attend tutorial meeting - 0.11                    |   [1 ]    |    [1 ]     |   [1 ]    |    [1 ]     |      [1 ]       |     [1 ]     |    [1 ]     |
| Attend group meeting - 0.11                       |   [1 ]    |    [1 ]     |   [1 ]    |    [1 ]     |      [1 ]       |     [1 ]     |    [1 ]     |
| Communicate with client - 0.11                    |   [1 ]    |    [1 ]     |   [1 ]    |    [1 ]     |      [1 ]       |     [1 ]     |    [1 ]     |
| Communicate with tutor - 0.11                     |   [ 1]    |    [1 ]     |   [1 ]    |    [ ]     |      [1 ]       |     [ ]     |    [ ]     |
| Communicate with shadow team - 0.11               |   [1 ]    |    [1 ]     |   [ 1]    |    [1 ]     |      [1 ]       |     [1 ]     |    [1 ]     |
| Represent agenda in meeting - 0.33                |   [1 ]    |    [1 ]     |   [1 ]    |    [ ]     |      [1 ]       |     [ ]     |    [ ]     |
| Write down thought on own initiative - 0.5        |   [ 1]    |    [1 ]     |   [ 1]    |    [1 ]     |      [1 ]       |     [ ]     |    [1 ]     |
| Complete job after distribute in meetings - 0.5   |   [1 ]    |    [1 ]     |   [1 ]    |    [1 ]     |      [1 ]       |     [1 ]     |    [1 ]     |
| Own distributed job and reflected in Gitlab - 0.5 |   [1 ]    |    [1 ]     |   [1 ]    |    [ ]     |      [ ]       |     [ ]     |    [ ]     |
| Positive in group chat and discussion - 0.8       |   [1 ]    |    [1 ]     |   [ 1]    |    [ 1]     |      [ ]       |     [ ]     |    [1 ]     |
| Come up with own idea - 0.2                       |   [1 ]    |    [ 1]     |   [ 1]    |    [ 1]     |      [ ]       |     [ ]     |    [1 ]     |
| Bonus for everyone - 1                            |   [x]    |    [x]     |   [x]    |    [x]     |      [x]       |     [x]     |    [x]     |
| Total score                                       |     5     |    5        |   1       |     4.05       |       3.5         |      3.55       |    4.05        |


# Week 6 (Audit Week)