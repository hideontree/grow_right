from importlib import import_module
import pandas as pd
import csv
import torch.nn.functional as F
import os
from torch.utils.data import Dataset, DataLoader
from transformers import DistilBertTokenizer, DistilBertForSequenceClassification, DistilBertModel, DistilBertTokenizerFast
import torch
import numpy as np
from transformers import BertTokenizer, BertModel, BertConfig, AdamW,get_linear_schedule_with_warmup
from sklearn.model_selection import train_test_split
import time
from torch import nn
from transformers import DistilBertTokenizer, DistilBertModel, DistilBertConfig

def concact_skill(config):
    # the skill dataset Mismatch with number of users
    df = pd.read_csv(config.skill_path, encoding='gbk', low_memory=False)
    print(df.head(5))

    index_list = df.index[df['ID'].notnull()].tolist() # find all index includes https
    print(len(index_list))
    count = 0
    for i in range(len(index_list)):
        if i+1 == len((index_list)):
            print(count)
            break
        else:
            count +=1
            start_idx = index_list[i]
            end_idx = index_list[i+1]

            temp = df.loc[start_idx:end_idx, ['Skill']]

            str_array = temp.values.flatten().astype(str)[:-1]  # list
            flat_str = ','.join(str_array)
            user_id = df.loc[start_idx, ['ID']].values.flatten().astype(str)
            user_id = user_id.tolist()[0]

            user_id = user_id.replace('[', '').replace(']', '')
            a_list = []
            a_list.append(user_id)
            a_list.append(flat_str)
            with open('./dataset/new_skill.csv', mode='a', newline='') as file:
                writer = csv.writer(file)
                writer.writerow(a_list)


def read_csv(config):
    education = pd.read_csv(config.education_path, encoding='gbk')
    interest = pd.read_csv(config.interest_path,encoding='gbk')
    skill = pd.read_csv('./dataset/new_skill.csv', encoding='gbk', low_memory=False)
    return education, interest, skill


def concatenate_feature(row):
    return ','.join([str(x) for x in row[1:]])


def combine_features(education, interest, skill):
    # the number of education & interest > skill
    if os.path.exists('./dataset/merge.csv'):
        merged_df = pd.read_csv('./dataset/merge.csv')
        print('merge data exist')
    else:
        merged_df = pd.merge(education, interest)
        merged_df = pd.merge(merged_df, skill)

        merged_df.to_csv('./dataset/merge.csv', index=False)
    if not os.path.exists('./dataset/selected_user_features.csv'):
        merged_df.fillna('', inplace=True)
        print('build user profile')
        # select user feature
        selected_df = merged_df[config.user_feautres]
        # if not os.path.exists('./dataset/selected_user_features.csv'):
        #     selected_df = selected_df.drop_duplicates(subset='ID',keep='first')
        #     # todo use multi features
        #     # combine selected feature into one text
        #     selected_df = selected_df.melt(id_vars=config.user_feautres[0],
        #                                    value_vars=config.user_feautres[1:], var_name='user_profile')['value']
        #     selected_df.to_csv('./dataset/selected_user_features.csv', index=False)
        selected_df = selected_df.drop_duplicates(subset='ID', keep='first')

        # combine selected feature into one text
        # selected_df = pd.melt(selected_df, id_vars=config.user_feautres[0],
        #                       value_vars=config.user_feautres[1:], var_name='user profile',
        #                       value_name='Text')
        selected_df['user_profile'] = selected_df.apply(concatenate_feature, axis=1)
        selected_df =selected_df[[selected_df.columns[0], 'user_profile']]
        selected_df.to_csv('./dataset/selected_user_features.csv', index=False)
    else:
        selected_df = pd.read_csv('./dataset/selected_user_features.csv')
        print('user profile exist')

    return selected_df


#--------preprocess-----------#
def cleaning(text):
    return " ".join(text.strip().split())

import random
def preprocess(config):
    """
    preprocess user profile and job description
    :param config:
    :return: two list
    """

    user_profiles = pd.read_csv(config.user_data)['user_profile'].tolist()[:60]
    job_desc = pd.read_csv(config.merge_job_path)['job description'].tolist()[:60]


    cleaned_profiles = [cleaning(profile) for profile in user_profiles]
    cleaned_jobs = [cleaning(job) for job in job_desc]
    random.shuffle(cleaned_jobs)
    random.shuffle(cleaned_profiles)
    return cleaned_profiles, cleaned_jobs
    # # (batch_size, sequence_length)
    # input_ids_user = config.tokenizer(cleaned_profiles, padding=True, truncation=True,
    #                              max_length=config.max_length, return_tensors="pt")["input_ids"]
    #
    # attention_mask_user = (input_ids_user != config.tokenizer.pad_token_id).float()
    #
    # input_ids_job = config.tokenizer(cleaned_jobs, padding=True, truncation=True,
    #                              max_length=config.max_length, return_tensors="pt")["input_ids"]
    # attention_mask_job = (input_ids_job != config.tokenizer.pad_token_id).float()


class MyDataset(Dataset):
    def __init__(self, config, profiles, job_descriptions):
        self.profiles = profiles
        self.job_descriptions = job_descriptions
        self.tokenizer = config.tokenizer
        self.max_length = config.max_length

    def __len__(self):
        return len(self.profiles) * len(self.job_descriptions)

    def __getitem__(self, index):
        resume_idx = index // len(self.job_descriptions)
        job_description_idx = index % len(self.job_descriptions)
        profile = self.profiles[resume_idx]
        job_description = self.job_descriptions[job_description_idx]

        encoding = self.tokenizer(
            text=profile,
            text_pair=job_description,
            add_special_tokens=True,
            max_length=self.max_length,
            truncation=True,
            padding='max_length',
            return_tensors='pt'
        )

        return encoding


def similarity(a, b, mode=0):
    # todo: other similarity
    if mode == 0:
        # cos similarity
        sim =  F.cosine_similarity(a, b, dim=-1).unsqueeze(-1)
    else:
        pass
    return sim

# todo
def evaluate(dataloader):
    total_loss = 0
    model.eval()
    with torch.no_grad():
        for batch in dataloader:
            input_ids = batch['input_ids'].squeeze(1).to(device)
            attention_mask = batch['attention_mask'].squeeze(1).to(device)

            outputs = model(input_ids=input_ids, attention_mask=attention_mask)
            user_embeddings = outputs['hidden_states'][-1][:, 0, :]
            job_embeddings = outputs['hidden_states'][-1][:, 1, :]

            similarities = torch.stack(
                [similarity(user_emb.unsqueeze(0), job_emb.unsqueeze(0)) for user_emb, job_emb in
                 zip(user_embeddings, job_embeddings)])
            loss = -torch.mean(similarities)
            total_loss += loss.item()

    return total_loss / len(dataloader)



if __name__ == '__main__':
    # concact_skill(config)
    x = import_module('models.' + 'Bert')
    config = x.Config()
    education, interest, skill = read_csv(config)
    selected_features = combine_features(education, interest, skill)
    cleaned_profiles, cleaned_jobs = preprocess(config)

    train_user_profiles, test_user_profiles = train_test_split(cleaned_profiles, test_size=0.2, random_state=42)
    train_dataset = MyDataset(config, train_user_profiles, cleaned_jobs)
    test_dataset = MyDataset(config, test_user_profiles, cleaned_jobs)
    train_dataloader = DataLoader(train_dataset, config.batch_size)
    test_dataloader = DataLoader(test_dataset, config.batch_size)
    criterion = nn.MSELoss()

    # configure other hyper-parameters
    best_test_loss = float('inf')
    model_save_path = "./save_dict/bert_model.pt"
    bert_config = DistilBertConfig.from_pretrained(config.model_name)
    bert_config.output_hidden_states = True
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(f"Using device: {device}")

    model = DistilBertModel.from_pretrained(config.model_name, config=bert_config)
    model.to(device)
    optimizer = AdamW(model.parameters(), lr=config.lr, weight_decay=config.weight_deacy)
    scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0,
                                                num_training_steps=len(train_dataloader) * config.num_epochs)

    # training
    for epoch in range(config.num_epochs):
        model.train()
        st = time.time()
        for batch in train_dataloader:
            optimizer.zero_grad()
            input_ids = batch['input_ids'].squeeze(1).to(device)
            attention_mask = batch['attention_mask'].squeeze(1).to(device)


            # compute embeddings
            outputs = model(input_ids=input_ids, attention_mask=attention_mask)
            user_embeddings = outputs['hidden_states'][-1][:, 0, :]
            job_embeddings = outputs['hidden_states'][-1][:, 1, :]
            user_embeddings = model
            # compute sim
            similarities = torch.stack(
                [similarity(user_emb.unsqueeze(0), job_emb.unsqueeze(0)) for user_emb, job_emb in
                 zip(user_embeddings, job_embeddings)])
            # loss = -torch.mean(similarities)
            loss = criterion()
            loss.backward()
            optimizer.step()
            scheduler.step()

        # compute test loss
        test_loss = evaluate(test_dataloader)
        end = time.time()
        print('epoch {}/{}, training loss: {:.3f}, test loss: {:.3f}, training time: {:.3f}'.format(epoch, config.num_epochs, loss.detach().cpu(), test_loss, end-st))
    a= 1