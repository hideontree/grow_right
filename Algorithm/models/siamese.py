from transformers import DistilBertTokenizer, DistilBertForSequenceClassification, DistilBertModel, DistilBertTokenizerFast
import torch
from transformers import AutoTokenizer, AutoModel, AdamW, get_linear_schedule_with_warmup

class Config(object):
    def __init__(self):
        self.num_clusters = 10
        self.job_description = './dataset/seek_australia.csv'
        self.education_path = './dataset/education.csv'
        self.skill_path = './dataset/skill.csv'
        self.interest_path = './dataset/interest.csv'
        self.user_data = './dataset/selected_user_features.csv'
        self.merge_job_path = './dataset/merge_job.csv'

        self.user_feautres = ['ID',
            'Degree1', 'Degree2', 'Field of Study1','Interest_1',
            'Interest_2','Interest_3', 'Interest_4', 'Interest_5', 'Interest_6', 'Skill'
                       ]
        self.job_features = ['url','category', 'city', 'company_name'
                             , 'job_description', 'job_title',
                             'salary_offered', 'state']


        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.model_name = '"distilbert-base-uncased'
        self.sbert_name = 'paraphrase-distilroberta-base-v2'
        # self.tokenizer = AutoTokenizer.from_pretrained(self.model_name)
        self.tokenizer = DistilBertTokenizerFast.from_pretrained('distilbert-base-uncased')

        self.max_length = 128
        self.batch_size = 2
        self.lr = 2e-5
        self.num_epochs = 10
        self.weight_deacy = 3e-5