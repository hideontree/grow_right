package com.growright.service.serviceImpl;

import com.growright.mapper.UserMapper;
import com.growright.pojo.User;
import com.growright.service.UserSerivce;
import com.growright.uitl.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserServiceImpl implements UserSerivce {


    @Autowired
    //@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    UserMapper userMapper;

    @Override
    public String loginService(String username, String password) {
        User user = userMapper.selectUserByUsername(username);
        if (user != null) {
            String pwd = user.getPassword();
            if (pwd.equals(password)) {
                log.info("Login success, generated token!");
                return JwtUtil.createToken(user);
            } else {
                log.info("Login failed, wrong password!");
                return "Wrong Password!";
            }
        }
        log.info("Login failed, wrong username!");
        return "Wrong Username";
    }

    @Override
    public String registerService(User user) {
        User checkUser = userMapper.selectUserByUsername(user.getUsername());
        if (checkUser == null) {
            if ("".equals(user.getPassword())) {
                return "Empty Password";
            } else if ("".equals(user.getEmail())) {
                return "Empty Email";
            } else if ("".equals(user.getUsername())) {
                return "Empty Username";
            } else {
                userMapper.insertUser(user);
                return "SUCCESS";
            }
        } else {
            return "User has been registered";
        }
    }
}
