package com.growright;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

//@SpringBootApplication
@SpringBootApplication
@ServletComponentScan(basePackages = "com.growright.uitl")
public class CareermatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(CareermatchApplication.class, args);
	}

}
