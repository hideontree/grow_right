package com.growright.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class SecureController {

    /**
     * For testing if jwt works
     */
    @RequestMapping("/secure/getUserInfo")
    public String login(HttpServletRequest request) {
        System.out.println(request);
        Integer id = (Integer) request.getAttribute("id");
        String username = request.getAttribute("username").toString();
        String password = request.getAttribute("password").toString();
        return "Current user information id=" + id +
                ", username=" + username +
                ", password=" + password;
    }
}
