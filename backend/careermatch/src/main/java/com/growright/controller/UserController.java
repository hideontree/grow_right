package com.growright.controller;


import com.growright.pojo.User;
import com.growright.service.serviceImpl.UserServiceImpl;
import com.growright.uitl.Result;
import com.growright.uitl.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserServiceImpl userServiceImpl;

    @RequestMapping("/hello")
    public String hello() {
        return "Hello world";
    }

//    @RequestMapping("/login")
//    public Result resultlogin(@RequestParam String username, @RequestParam String password) {
//        String msg = userServiceImpl.loginService(username, password);
//        if (("Success").equals(msg)) {
//            return ResultUtil.success("Successfully login");
//        } else {
//            return ResultUtil.error(msg);
//        }
//    }

    @RequestMapping("/login")
    public String login(@RequestParam String username, @RequestParam String password) {
        String token = userServiceImpl.loginService(username, password);
        if (("Wrong Password!").equals(token) || ("Wrong Username!").equals(token)) {
            return "";
        } else {
            return token;
        }
    }


    @RequestMapping("/register")
    public Result register (@RequestBody User user) {
        if (user == null ||
                user.getEmail() == null ||
                user.getFirstName() == null ||
                user.getLastName() == null ||
                user.getUsername() == null ||
                user.getPassword() == null ) {
            return ResultUtil.error("Missing Information");
        }
        String msg = userServiceImpl.registerService(user);
        if (("SUCCESS").equals(msg)) {
            return ResultUtil.success("Successfully registered");
        } else {
            return ResultUtil.error(msg);
        }
    }

}
