package com.growright.mapper;

import com.growright.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName: UserMapper
 * Description:
 *
 * @Author YH Su
 * @Create 19/8/2023 11:58 am
 * @Version 1.0
 */

@Mapper
@Repository
public interface UserMapper {
    List<User> list();

    User selectUserByUsername(String username);
    User selectUserByEmail(String email);
    void insertUser(User user);

}
