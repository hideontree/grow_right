package com.growright;

import com.growright.mapper.UserMapper;
import com.growright.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class CareermatchApplicationTests {

	@Autowired
	private UserMapper userMapper;

	@Test
	void contextLoads() {
	}

	@Test
	public void testListUser() {
		List<User> userList = userMapper.list();
		userList.stream().forEach(user -> {
			System.out.println(user);
		});

	}

}
