package com.growright;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CareermatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(CareermatchApplication.class, args);
	}

}
