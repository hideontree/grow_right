import { NextResponse } from 'next/server';
import jwt from 'jsonwebtoken';

interface LoginRequestData {
  username: string;
  password: string;
}

export async function POST(request: Request): Promise<NextResponse> {
  const { username, password } = await request.json() as LoginRequestData;
  console.log(username, password);
  console.log(process.env.APP_USERNAME);
  console.log(process.env.APP_PASSWORD);

  if (username === process.env.APP_USERNAME && password === process.env.APP_PASSWORD) {
    const token = jwt.sign({ username }, process.env.JWT_SECRET!, { expiresIn: '1h' });
    return NextResponse.json({ token });
  } else {
    return NextResponse.json({ error: 'Invalid credentials' , status: '401'});
  }
}