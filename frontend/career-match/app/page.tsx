"use client";
import JobCard from "@/components/JobCard";
import Navbar from "@/components/Navbar";
import { Button } from "@/components/ui/button";
import Image from "next/image";
import Link from "next/link";
import Footer from "@/components/Footer";
import Pagnination from "@/components/Pagnination";
import { useState, useEffect } from "react";
import { Skeleton } from "@/components/ui/skeleton";

const jobItems = [
  {
    jobTitle: "Senior Full Stack Backend Engineer",
    jobField: "Engineering",
    jobType: "Full-time",
    location: "Canberra, Australia",
  },
  {
    jobTitle: "Junior Frontend Developer",
    jobField: "Design",
    jobType: "Part-time",
    location: "Sydney, Australia",
  },
  {
    jobTitle: "Data Scientist",
    jobField: "Data Analysis",
    jobType: "Full-time",
    location: "Melbourne, Australia",
  },
  {
    jobTitle: "Product Manager",
    jobField: "Management",
    jobType: "Full-time",
    location: "Brisbane, Australia",
  },
  {
    jobTitle: "UX Designer",
    jobField: "Design",
    jobType: "Part-time",
    location: "Perth, Australia",
  },
  {
    jobTitle: "Database Administrator",
    jobField: "IT",
    jobType: "Full-time",
    location: "Adelaide, Australia",
  },
  {
    jobTitle: "DevOps Engineer",
    jobField: "Engineering",
    jobType: "Full-time",
    location: "Hobart, Australia",
  },
  {
    jobTitle: "Marketing Specialist",
    jobField: "Marketing",
    jobType: "Part-time",
    location: "Darwin, Australia",
  },
  {
    jobTitle: "HR Manager",
    jobField: "Human Resources",
    jobType: "Full-time",
    location: "Gold Coast, Australia",
  },
  {
    jobTitle: "Financial Analyst",
    jobField: "Finance",
    jobType: "Full-time",
    location: "Newcastle, Australia",
  },
];

export default function Home() {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsLoading(false);
    }, 1500);

    // 清除定时器，防止组件卸载后还执行setState
    return () => clearTimeout(timer);
  }, []); // 空依赖数组表示这个effect只会在组件挂载时运行一次
  return (
    <>
      <Navbar />
      <h1 className="w-full text-center text-3xl font-bold bg-gray-50 py-10">
        Here are your recommended jobs
      </h1>
      {jobItems.map((jobItem, index) =>
        isLoading ? (
          <div
            key={index}
            className="w-full flex items-center justify-center py-4"
          >
            <Skeleton className="w-[200px] h-[20px] rounded-full bg-gray-50" />
          </div>
        ) : (
          <JobCard
            key={index}
            jobTitle={jobItem.jobTitle}
            jobType={jobItem.jobType as "Full-time" | "Part-time"}
            jobField={jobItem.jobField}
            location={jobItem.location}
          />
        )
      )}
      <div className="flex w-full justify-center py-8 bg-gray-50">
        <Pagnination />
      </div>

      <Footer />
    </>
  );
}
