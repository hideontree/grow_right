import { createContext } from 'react';

export interface LoggedInContextProps {
  isLoggedIn: boolean;
  login: () => void;
  logout: () => void;
}

export const LoggedInContext = createContext<LoggedInContextProps | null>(null);