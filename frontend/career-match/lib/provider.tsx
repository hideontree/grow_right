"use client";

import React, { useState, createContext } from "react";
import { QueryClientProvider, QueryClient } from "@tanstack/react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import { ReactQueryStreamedHydration } from "@tanstack/react-query-next-experimental";
import { LoggedInContext, LoggedInContextProps } from "./contexts";

function Providers({ children }: React.PropsWithChildren) {
  const [client] = React.useState(new QueryClient());
  const [isLoggedIn, setLoggedIn] = useState(false);
  const login = () => {
    setLoggedIn(true);
  };
  const logout = () => {
    setLoggedIn(false);
  };
  const contextValue: LoggedInContextProps = {
    isLoggedIn,
    login,
    logout,
  };

  return (
    <QueryClientProvider client={client}>
      <LoggedInContext.Provider value={contextValue}>
        <ReactQueryStreamedHydration>{children}</ReactQueryStreamedHydration>
      </LoggedInContext.Provider>
      {/* <ReactQueryDevtools initialIsOpen={false} /> */}
    </QueryClientProvider>
  );
}

export default Providers;
