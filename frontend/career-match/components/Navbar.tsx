"use client";
import Image from "next/image";
import logoImage from "../public/logo.jpg";
import MainNav from "./MainNav";
import UserSheet from "./UserSheet";
import Link from "next/link";
import { Button } from "./ui/button";
import { useContext } from "react";
import { LoggedInContext } from "@/lib/contexts";

const Navbar = () => {
  const loggedIn = useContext(LoggedInContext);

  return (
    <div className="">
      <div className="border-b flex h-16 justify-between items-center px-4">
        <div className="flex items-center">
          <Image src={logoImage} height={50} width={50} alt="logo" />
          <p className="font-bold">CareerMatch</p>
          <MainNav className="hidden ml-6 md:block" />
        </div>
        <div className="gap-4 flex">
          {loggedIn?.isLoggedIn ? (
            <UserSheet />
          ) : (
            <Link href="/auth/sign-in">
              <Button variant="outline">Sign In</Button>
            </Link>
          )}
        </div>
      </div>
    </div>
  );
};

export default Navbar;
