"use client";

import { useState } from "react";
import Link, { LinkProps } from "next/link";
import { useRouter } from "next/navigation";
import { cn } from "@/lib/utils";
import { Sheet, SheetContent, SheetTrigger } from "./ui/sheet";
import { Button, buttonVariants } from "./ui/button";
import { Avatar, AvatarFallback } from "./ui/avatar";

const UserSheet = () => {
  const [open, setOpen] = useState(false);
  return (
    <Sheet open={open} onOpenChange={setOpen}>
      <SheetTrigger asChild>
        <Avatar className="cursor-pointer">
          <AvatarFallback>CM</AvatarFallback>
        </Avatar>
      </SheetTrigger>
      <SheetContent side="right">
        <h1 className="text-2xl font-bold mt-8">Welcome, Jack!</h1>
        <div className="flex flex-col space-y-3 mt-4">
          <MobileLink href="/" onOpenChange={setOpen}>
            Edit Profile
          </MobileLink>
          <MobileLink href="/" onOpenChange={setOpen}>
            View My Resume
          </MobileLink>
          <MobileLink href="/" onOpenChange={setOpen}>
            View My Resume
          </MobileLink>
        </div>
        <MobileLink
          href="/auth/sign-in"
          onOpenChange={setOpen}
          className={cn("mt-4", buttonVariants({ variant: "destructive" }))}
        >
          Sign Out
        </MobileLink>
      </SheetContent>
    </Sheet>
  );
};

interface MobileLinkProps extends LinkProps {
  onOpenChange?: (open: boolean) => void;
  children: React.ReactNode;
  className?: string;
}

const MobileLink = ({
  href,
  onOpenChange,
  className,
  children,
  ...props
}: MobileLinkProps) => {
  const router = useRouter();
  return (
    <Link
      href={href}
      onClick={() => {
        router.push(href.toString());
        onOpenChange?.(false);
      }}
      className={cn(className)}
      {...props}
    >
      {children}
    </Link>
  );
};

export default UserSheet;
