"use client";

import { cn } from "@/lib/utils";
import { Icons } from "./Icons";
import { Button } from "./ui/button";
import { Input } from "./ui/input";
import { Label } from "./ui/label";
import { PasswordInput } from "./PasswordInput";
import axios from "axios";
import Providers from "@/lib/provider";
import Cookies from "js-cookie";

import * as React from "react";
import { useRouter } from "next/navigation";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import type { z } from "zod";
import { useMutation } from "@tanstack/react-query";

import { loginSchema } from "@/lib/validations/auth";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { LoggedInContext } from "@/lib/contexts";

interface LoginFormProps extends React.HTMLAttributes<HTMLDivElement> {}
type Inputs = z.infer<typeof loginSchema>;
const serverURL = process;

export function LoginForm({ className, ...props }: LoginFormProps) {
  const router = useRouter();
  const auth = React.useContext(LoggedInContext);

  // react-hook-form
  const form = useForm<Inputs>({
    resolver: zodResolver(loginSchema),
    defaultValues: {
      username: "",
      password: "",
    },
  });

  const signInMutation = useMutation({
    mutationFn: (loginData: Inputs) => {
      console.log(serverURL);
      return axios
        .post(`/api/login`, {
          username: loginData.username,
          password: loginData.password,
        })
        .then((response) => {
          console.log("Response:", response.data);
          console.log(loginData.username, "+", loginData.password);

          // 检查响应中是否有 token，并将其存储在 cookie 中
          if (response.data && response.data.token) {
            Cookies.set("token", response.data.token);
            auth?.login();
            router.replace("/");
          }
        })
        .catch((error) => {
          // 在这里处理请求错误
          console.error("Error:", error);
        })
        .finally(() => {});
    },
  });

  function onSubmit(data: Inputs) {
    signInMutation.mutate(data);
  }

  return (
    <Form {...form}>
      <form
        className="grid gap-4"
        onSubmit={(...args) => void form.handleSubmit(onSubmit)(...args)}
      >
        <FormField
          control={form.control}
          name="username"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Username</FormLabel>
              <FormControl>
                <Input placeholder="Enter your username" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="password"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Password</FormLabel>
              <FormControl>
                <PasswordInput placeholder="**********" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <Button disabled={signInMutation.isPending}>
          {signInMutation.isPending && (
            <Icons.spinner
              className="mr-2 h-4 w-4 animate-spin"
              aria-hidden="true"
            />
          )}
          Sign in
          <span className="sr-only">Sign in</span>
        </Button>
      </form>
    </Form>
    // <div className={cn("grid gap-6", className)} {...props}>
    //   <form onSubmit={onSubmit}>
    //     <div className="grid gap-6">
    //       <div className="grid gap-4">
    //         <Label className="sr-only" htmlFor="email">
    //           Email
    //         </Label>
    //         <Input
    //           id="email"
    //           placeholder="name@example.com"
    //           type="email"
    //           autoCapitalize="none"
    //           autoComplete="email"
    //           autoCorrect="off"
    //           disabled={isLoading}
    //         />
    //         <Input
    //           id="email"
    //           placeholder="password"
    //           type="email"
    //           autoCapitalize="none"
    //           autoComplete="email"
    //           autoCorrect="off"
    //           disabled={isLoading}
    //         />
    //       </div>
    //       <Button disabled={isLoading}>
    //         {isLoading && (
    //           <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />
    //         )}
    //         Sign In with Email
    //       </Button>
    //     </div>
    //   </form>
    // </div>
  );
}
