import { NextResponse } from 'next/server';
import type { NextRequest } from 'next/server';
import { verifyAuth } from './lib/auth';

// This function can be marked `async` if using `await` inside
export async function middleware(request: NextRequest) {
  const token = request.cookies.get('token')?.value;
  const verifiedToken = token && (await verifyAuth(token).catch((error) =>{
    console.error(error);
  }));

  if(request.nextUrl.pathname.startsWith('auth/sign-in') && !verifiedToken) {
    return;
  }

  if(!verifiedToken){
    return NextResponse.redirect(new URL('/auth/sign-in', request.url));
  }
}
 
// See "Matching Paths" below to learn more
export const config = {
  matcher: ['/'],
}